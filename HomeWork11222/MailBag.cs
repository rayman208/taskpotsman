﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork11222
{
    class MailBag
    {
        Mail[] _mails;

        public MailBag(int n)
        {
            _mails = new Mail[n];
        }

        public void PushMail(int index, Mail mail)
        {
            _mails[index] = mail;
        }

        public void Print()
        {
            for (int i = 0; i < _mails.Length; i++)
            {
                if (_mails[i] != null)
                {
                    Console.WriteLine($"Письмо {i}:");
                    _mails[i].Print();
                    Console.WriteLine("--------------");
                }
            }
        }
    }
}
