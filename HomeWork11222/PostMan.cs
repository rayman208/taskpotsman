﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork11222
{
    class PostMan
    {
        MailBag[] _mbags;

        public PostMan()
        {
            _mbags = new MailBag[2];

            _mbags[0] = new MailBag(3);
            _mbags[1] = new MailBag(5);
        }

        public void AddMailToMailBag(int iBag, int iMail, Mail mail)
        {
            _mbags[iBag].PushMail(iMail, mail);
        }

        public void PrintBag(int iBag)
        {
            Console.WriteLine($"Сумка {iBag}:");
            Console.WriteLine("=================");
            _mbags[iBag].Print();
            Console.WriteLine("=================");
            Console.WriteLine();
        }

        public void PrintAllBags()
        {
            for (int i = 0; i < _mbags.Length; i++)
            {
                PrintBag(i);
            }
        }
    }
}
