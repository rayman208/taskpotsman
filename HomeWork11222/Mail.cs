﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork11222
{
    class Mail
    {
        private string _text;
        private string _address;
        private string _addressCopy;

        public Mail(string text, string address, string addressCopy)
        {
            _text = text;
            _address = address;
            _addressCopy = addressCopy;
        }

        public void Print()
        {
            Console.WriteLine("Текст: "+_text);
            Console.WriteLine("Адрес: "+_address);
            Console.WriteLine("Скрытый Адрес: " + _addressCopy);
        }
    }
}
